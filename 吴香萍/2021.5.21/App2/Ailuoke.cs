﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App2
{
    class Ailuoke:Hero
    {
        //字段：角色名字，角色介绍，角色昵称，攻击力，防御力，速度。
        //方法：每个角色都有三个不同的攻击技能。
        public string nickname;

        public Ailuoke()
        {

        }
        public Ailuoke(string name, string introduction, string nickname, int attack, int defence, int speed, string skill1, string skill2, string skill3):
            base(name,introduction,attack,defence,speed,skill1,skill2,skill3)
        {
            this.nickname = nickname;
        }

        public void Print()
        {
            base.Print();
            Console.WriteLine("角色昵称:"+nickname);
        }

    }
}
