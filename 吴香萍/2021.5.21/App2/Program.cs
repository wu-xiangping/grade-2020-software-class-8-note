﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App2
{
    class Program
    {
        //字段：角色名字，角色介绍，角色昵称，攻击力，防御力，速度。
        //方法：每个角色都有三个不同的攻击技能。
        static void Main(string[] args)
        {
            Ailuoke ailuoke = new Ailuoke("埃洛科","埃洛科是一名来自末日边境的勇士","埃",95,70,50,"碎石打击","烈焰锚钩","战斗咆哮");
            Console.WriteLine("第一位英雄：");
            ailuoke.Print();
            Console.WriteLine();

            Taila taila = new Taila("泰拉", "泰拉是为复仇而来的勇士", "泰", 85, 65, 50, "巨浪冲击", "元素突击", "复仇杀戮");
            Console.WriteLine("第二位英雄：");
            taila.Print();
            Console.WriteLine();

            Lukasi lukasi = new Lukasi("卢卡斯", "卢卡斯是一位彬彬有礼的刺客", "卢", 90, 55, 65, "减速陷阱", "能量浪潮", "旋风剑舞");
            Console.WriteLine("第三位英雄：");
            lukasi.Print();
            Console.WriteLine();

            Luofei luofei = new Luofei("洛非", "洛非是一名攻击迅猛且擅长传送魔法的时空旅行者", "洛", 70, 45, 80, "能量精灵", "暗影传送", "时空迸裂");
            Console.WriteLine("第四位英雄：");
            luofei.Print();
            Console.WriteLine();

            Console.ReadKey();
        }
    }
}
