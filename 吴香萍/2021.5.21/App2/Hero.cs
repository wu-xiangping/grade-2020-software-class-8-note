﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App2
{
    class Hero
    {
        //字段：角色名字，角色介绍，角色昵称，攻击力，防御力，速度。
        //方法：每个角色都有三个不同的攻击技能。

        public string name;
        public string introduction;
        public int attack;
        public int defence;
        public int speed;
        public string skill1;
        public string skill2;
        public string skill3;

        public Hero()
        {

        }
        public Hero(string name, string introduction, int attack, int defence, int speed, string skill1, string skill2, string skill3)
        {
            this.name = name;
            this.introduction = introduction;
            this.attack = attack;
            this.defence = defence;
            this.speed = speed;
            this.skill1 = skill1;
            this.skill2 = skill2;
            this.skill3 = skill3;
        }


        public void Print()
        {

            Console.WriteLine("角色名字" +name);
            Console.WriteLine("角色介绍" +introduction);
            Console.WriteLine("攻击力" +attack);
            Console.WriteLine("防御力" +defence);
            Console.WriteLine("速度" +speed);
            Console.WriteLine("技能1" +skill1);
            Console.WriteLine("技能2" +skill2);
            Console.WriteLine("技能3" +skill3);
        }

    }
}
