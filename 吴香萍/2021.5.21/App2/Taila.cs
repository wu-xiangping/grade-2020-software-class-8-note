﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App2
{
    class Taila : Hero
    {
        public string nickname;

        public Taila()
        {

        }
        public Taila(string name, string introduction, string nickname, int attack, int defence, int speed, string skill1, string skill2, string skill3) :
            base(name, introduction, attack, defence, speed, skill1, skill2, skill3)
        {
            this.nickname = nickname;
        }

        public void Print()
        {
            base.Print();
            Console.WriteLine("角色昵称:" + nickname);
        }

    }
}
