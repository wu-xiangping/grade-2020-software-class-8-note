﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1
{
    class Program
    {
        static void Main(string[] args)
        {
            Student student = new Student(001,"张三","男","350722230123564896", "13112012365","软件开发","20级");

            Console.WriteLine("学生信息：");
            student.Print();

            Teacher teacher = new Teacher(111,"李开发","男", "590122230123564896","15152012365","任课老师","233628507");
            Console.WriteLine("教师信息：");
            teacher.Print();
            Console.ReadKey();

        }
    }
}
