﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1
{
    class Person
    {
        public int id;
        public string name;
        public string sex;
        public string cardid;
        public string tel;

        public Person()
        {

        }
        public Person(int id, string name, string sex, string cardid, string tel)
        {
            this.id = id;
            this.name = name;
            this.sex = sex;
            this.cardid = cardid;
            this.tel = tel;
        }


        public void Print()
        {
            Console.WriteLine("编号：" + id);
            Console.WriteLine("姓名：" + name);
            Console.WriteLine("性别：" + sex);
            Console.WriteLine("身份证号：" + cardid);
            Console.WriteLine("联系方式：" + tel);
        }
    }
}
