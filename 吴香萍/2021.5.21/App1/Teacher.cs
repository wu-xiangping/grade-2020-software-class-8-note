﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1
{
    class Teacher:Person
    {
        //教师的字段：编号（Id）、姓名（Name）,性别（Sex）、身份证号（Cardid）、联系方式（Tel）、职称（Title）、工资号（Wageno）
        public string title;
        public string wageno;

        public Teacher()
        {

        }
        public Teacher(int id, string name, string sex, string cardid, string tel, string title, string wageno) : base(id, name, sex, cardid, tel)
        {
            this.title = title;
            this.wageno = wageno;
        }


        public  void Print()
        {
            base.Print();
            Console.WriteLine("职称：" + title);
            Console.WriteLine("工资号：" + wageno);
        }

    }
}
