﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1
{
    class Student:Person
    {
        //学生的字段：编号（Id）、姓名（Name）、性别（Sex）、身份证号（Cardid）、联系方式（Tel）、专业（Major）、年级（Grade）
        public string major;
        public string grade;

        public Student()
        {

        }
        public Student(int id, string name, string sex, string cardid, string tel,string major, string grade):base(id,name,sex,cardid,tel)
        {
            this.major = major;
            this.grade = grade;
        }

        public void Print()
        {
            base.Print();
            Console.WriteLine("专业：" + major);
            Console.WriteLine("年级：" + grade);
        }
    }
}
