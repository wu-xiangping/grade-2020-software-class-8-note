create table HOUSE_TYPE (
typeid int not null primary key identity(1,1),--类型编号
typename varchar(50) not null unique, --类型名称
);
create table HOUSE (
houseid int not null primary key identity(1,1), ---房屋编号
housename varchar(50) not null, --房屋名称
houseprice float not null default'0',
typeid int foreign key (typeid) references HOUSE_TYPE(typeid)
);
insert into HOUSE_TYPE (typename)
values ('小户型'),
		('经济型'),
		('别墅');
insert into HOUSE(housename,houseprice,typeid)
values ('花果山',5000,1),
		('天庭',15000,3),
		('十八层地狱',9000,2);

--查询所有房屋信息
select * from HOUSE

--使用模糊查询包含”型“字的房屋类型信息
select * from HOUSE_TYPE where typename like '%型'

--查询出房屋的名称和租金，并且按照租金降序排序
select housename,houseprice from HOUSE  order by  houseprice desc

--使用连接查询，查询信息，显示房屋名称和房屋类型名称
select HOUSE.housename,HOUSE_TYPE.typename from HOUSE
inner join HOUSE_TYPE on HOUSE.typeid=HOUSE_TYPE.typeid

--查询所有房屋中月租最高的房屋，显示最高的租金和房屋名称
select HOUSE.housename,HOUSE.houseprice from HOUSE where houseprice in (select max(houseprice) from HOUSE)
