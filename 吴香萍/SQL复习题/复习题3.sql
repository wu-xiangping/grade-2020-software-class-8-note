create database StarManagerDB

create table StarType(
T_NO int not null primary key identity(1,1),
T_NAME	nvarchar(20)
);

create table StarInfo(
S_NO int not null primary key identity(1,1),
S_NAME nvarchar(20) not null,
S_AGE int not null,
S_HOBBY nvarchar(20),
S_NATIVE nvarchar(20)default'中国大陆',
S_T_NO int foreign key (S_T_NO) references StarType (T_NO)
);

insert into StarType(T_NAME)
	values('体育明星'),
	('IT明星'),
	('相声演员');

insert into StarInfo(S_NAME,S_AGE,S_HOBBY,S_NATIVE,S_T_NO)
  values('梅西',30,'射门','阿根廷',1),
  ('科比',35,'过人','美国',1),
  ('蔡景现',40,'蔡景现','蔡景现',2),
  ('马斯克',36,'造火箭','外星人',2),
  ('郭德纲',50,'相声','中国',3),
  ('黄铮',41,'拼多多','中国',2);

--3、查询年龄最大的3个明星的姓名，特技和籍贯信息，要求使用别名显示列名。
select top 3 S_NAME,S_HOBBY,S_NATIVE from StarInfo order by S_AGE desc

--4、按明星类型编号分类查询明星人数，明星平均年龄，显示明星人数大于2的分组信息，要求使用别名显示列名。
select S_T_NO, count(S_NAME) 明星人数, avg(S_AGE) 平均年龄 from StarInfo
group by S_T_NO
having count(S_NAME)>2

--5、查询明星类型为“体育明星”中年龄最大的姓名、特技、籍贯信息，要求显示列别名。
select S_NAME 姓名,S_HOBBY 特技,S_NATIVE 籍贯 from StarInfo 
where S_AGE in (select max(S_AGE) from StarInfo where S_T_NO=1)
