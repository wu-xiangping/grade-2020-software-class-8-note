create table Class(
 ClassId int primary key identity(1,1),
 ClassName nvarchar(50) not null unique,
);
insert into Class(ClassName) 
    values ('软件八班');
insert into Class(ClassName) 
    values ('软件七班');

select * from Class;


create table Student (
    StudentId int primary key identity(1,1),
    StudentName nvarchar(50) not null,
    StudentSex tinyint not null default '3',
    StudentBirth date,
    StudentAddress nvarchar(255) not null default '',
    ClassId int not null,
);
alter table Student add StudentIdentityCard varchar(20) not null  default '';
alter table Student add constraint DF_Student_ClassId default(0) for ClassId;
alter table Student add constraint CK_Student_StudentSex check(StudentSex=1 or StudentSex=2 or StudentSex=3);

insert into Student (StudentName, StudentBirth, StudentSex, StudentAddress) 
    values ('刘正','2000-01-01','广西省桂林市七星区空明西路10号鸾东小区');
insert into Student (StudentName, StudentBirth, StudentAddress) 
    values ('甲贷','2002-01-01','广西省桂林市七星区空明西路10号鸾西小区');
insert into Student (StudentName, StudentBirth) 
    values ('雄安','2002-01-01');

	select * from Student;
	
create table Course(
	CourseId int primary key identity(1,1),
	CourseName nvarchar(50) not null unique,
	CourseCredit tinyint not null default '0',
);
alter table Course add constraint CK_Course_CourseCredit check(CourseCredit>=0);

insert into Course (CourseName, CourseCredit)
    values ('专业','10');
insert into Course (CourseName, CourseCredit)
    values ('英语');

	select * from Course;

create table ClassCourse(
    ClassCourseId int primary key identity(1,1),
	ClassId int not null,
	CourseId int not null,
);
alter table ClassCourse add constraint FK_ClassCourse_ClassId foreign key (ClassId) references Class(ClassId);
alter table ClassCourse add constraint FK_ClassCourse_CourseId foreign key (CourseId) references Course(CourseId);
alter table Score add constraint FK_Score_StudentId foreign key (StudentId) references Student(StudentId);
alter table Score add constraint FK_Score_CourseId foreign key (CourseId) references Course(CourseId);

insert into ClassCourse (ClassId, CourseId)
    values ('1','1');

	select * from ClassCourse;

create table Score(
    ScoreId int primary key identity(1,1),
	StudentId int not null,
	CourseId int not null,
	Score int not null,
);
alter table Score add IsResit tinyint not null  default '0';--0不是，1是
alter table Score add constraint CK_Score_Score check(Score>=0);

insert into Score (StudentId, CourseId, Score)
    values ('8','1','60');

select * from Score;