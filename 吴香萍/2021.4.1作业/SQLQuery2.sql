create table stuinfo(
   stuNO nvarchar(20) not null,
   stuName nvarchar(50),
   stuAge int not null,
   stuAdress nvarchar(55) not null,
   stuSeat int identity(1, 1),
   stuSex tinyint not null,
);

create table stuexam(
  examNo int not null identity(1, 1),
  stuNO nvarchar(20) not null,
  writtenExam int,
  labExam int,
);

insert into stuinfo(stuNO, stuName, stuAge, stuAdress, stuSex)
	values('s2501','张秋利','20','美国硅谷','1'),
	('s2502','李斯文','18','湖北武汉','0'),
	('s2503','马文才','22','湖南长沙','1'),
	('s2504','欧阳俊雄','21','湖北武汉','0'),
	('s2505','梅超风','20','湖北武汉','1'),
	('s2506','陈旋风','19','美国硅谷','1'),
	('s2507','陈风','20','美国硅谷','0');

insert into stuexam(stuNO, writtenExam, labExam)
	values('s2501','50','70'),
	('s2502','60','65'),
	('s2503','86','85'),
	('s2504','40','80'),
	('s2505','70','90'),
	('s2506','85','90');


	select * from stuinfo;
	select * from stuexam;

	--查询学生信息表（stuinfo）中所有列信息，给每列取上中文名称
	select stuNO 学号,stuName 姓名,stuAge 年龄,stuAdress 地址,stuSeat 座位号,stuSex 性别 from stuinfo;

	--查询学生信息表（stuinfo）中的姓名，年龄和地址三列的信息
	select  stuName 名字, stuAge 年龄, stuAdress 地址 from stuinfo;

	--查询学生分数表（stuexam）中的学号，笔试和机试三列的信息，并为这三列取中文名字,注意：要用三种方法
	select stuNo 学号, writtenExam 笔试, labExam 机试 from stuexam 
	select stuNo as 学号, writtenExam as 笔试, labExam as 机试 from stuexam 
	select 学号=stuNo ,  笔试=writtenExam, 机试=labExam  from stuexam 
	
	--查询学生信息表（stuInfo）中的学号，姓名，地址，以及将：姓名+@+地址 组成新列 “邮箱”
	select stuNO 学号, stuName 名字, stuAdress 地址, stuName+'@'+stuAdress 邮箱 from stuinfo;

	--查询学生分数表（stuexam）中的学生的学号，笔试，机试以及总分这四列的信息
	select stuNO 学号, writtenExam 笔试, labExam 机试, writtenExam + labExam 总分 from stuexam 


	--8查询学生信息表（stuInfo）中前3行记录 
	select top 3 * from stuinfo;

	--9查询学生信息表（stuInfo）中前4个学生的姓名和座位号
	select top 4  stuName 名字, stuSeat 座位号 from stuinfo

	--10查询学生信息表（stuInfo）中一半学生的信息
	select top 50 percent * from stuinfo

	--11将地址是湖北武汉，年龄是20的学生的所有信息查询出来
	select * from stuinfo where stuAdress='湖北武汉'and stuage='20'

	--12将机试成绩在60-80之间的信息查询出来，并按照机试成绩降序排列（用两种方法实现）
	select * from stuexam where labExam >=60 and labExam <=80 order by labExam desc
	select * from stuexam where labExam between '60' and '80' order by labExam desc

	--13查询来自湖北武汉或者湖南长沙的学生的所有信息（用两种方法实现）
	select * from stuinfo where stuAdress ='湖北武汉' or stuAdress ='湖南长沙'
	select * from stuinfo where stuAdress like '湖北%' or stuAdress like '湖南%'

	--14查询出笔试成绩不在70-90之间的信息,并按照笔试成绩升序排列（用两种方法实现）
	select * from stuexam where writtenExam <70and writtenExam <90 order by writtenExam asc
	select * from stuexam where writtenExam not between '70'and '90'order by writtenExam asc

	--15查询年龄没有写的学生所有信息
	select *from stuinfo where stuAge is null

	--16查询年龄写了的学生所有信息
	select *from stuinfo where stuAge is not null

	--17.查询姓张的学生信息
	select *from stuinfo where stuName like '张%'

	--18.查询学生地址中有‘湖’字的信息
	select *from stuinfo where stuName like '%湖%'

	--19.查询姓张但名为一个字的学生信息
	select *from stuinfo where stuName like '张_'

	--20.查询姓名中第三个字为‘俊’的学生的信息，‘俊’后面有多少个字不限制
	select *from stuinfo where stuName like '__俊%'

	--21.按学生的年龄降序显示所有学生信息
	select * from stuinfo order by stuAge desc

	--22.按学生的年龄降序和座位号升序来显示所有学生的信息
	select * from stuinfo order by stuSeat asc , stuAge desc
	
	--23显示笔试第一名的学生的考试号，学号，笔试成绩和机试成绩
	select top 1 * from stuexam order by writtenExam desc
	
	--24.显示机试倒数第一名的学生的考试号，学号，笔试成绩和机试成绩
	select top 1 * from stuexam order by labExam asc