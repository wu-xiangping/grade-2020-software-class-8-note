﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp7
{
    class Program
    {
        static void Main(string[] args)
        {
            //7.	定义一个方法，实现一维数组的排序功能，从大到小排序
            int[] a = { 5, 1, 8, 3, 4 };

            by(a);
            Console.WriteLine("从大到小排序为：");
            foreach (int item in a)
            {
               
                Console.WriteLine(item);
            }
            
            Console.ReadKey();
        }

        private static void by(int[] a)
        {
            Array.Sort(a);
            Array.Reverse(a);

        }
    }
}
