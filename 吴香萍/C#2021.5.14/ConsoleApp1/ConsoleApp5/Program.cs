﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp5
{
    class Program
    {
        static void Main(string[] args)
        {
            // 5.在 Main 方法中创建一个字符串类型的数组，并存入 5 个值，然后将数组中下标是偶数的元素输出。
            string[] a = { "a","b","c","d","e" };
            for (int i = 0; i <a. Length; i=i+2)
            {
                Console.WriteLine(i+"下标是偶数的元素");
            }
            Console.ReadKey();
        }
    }
}
