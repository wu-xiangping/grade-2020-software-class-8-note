﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp6
{
    class Program
    {
        static void Main(string[] args)
        {
            //6.在 Main 方法中创建一个 double 类型的数组，
            //并在该数组中存入 5 名学生的考试成绩，计算总成绩和平均成绩。（要求使用foreach语句实现该功能）

            double[] score = { 66,85,75,90,60 };
            double sum = 0;
            double avg = 0;
            foreach (var item in score)
            {
                sum = sum + item;
                avg = sum / 5;
                
            }
                Console.WriteLine("总成绩为："+sum);
                Console.WriteLine("平均分为："+avg);
                Console.ReadKey();
        }
    }
}
