﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
            //4.接受用户输入一个字符；然后判断这个字母是否为元音字母（不区分大小写），
            //  元音字母为A、E、I、O、U。用switch case实现；
            Console.WriteLine("用户输入一个大写字符：");
            char num = char.Parse(Console.ReadLine());
            switch (num)
            {
                case 'A':
                Console.WriteLine(num+"是元音字母");
                break;
                case 'E':
                Console.WriteLine(num + "是元音字母");
                break;
                case 'I':
                Console.WriteLine(num + "是元音字母");
                break;
                case 'O':
                Console.WriteLine(num + "是元音字母");
                break;
                case 'U':
                Console.WriteLine(num + "是元音字母");
                break;
         

                default:
                Console.WriteLine(num + "不是元音字母");
                break;
            }
            Console.ReadKey();
        }
    }
}
