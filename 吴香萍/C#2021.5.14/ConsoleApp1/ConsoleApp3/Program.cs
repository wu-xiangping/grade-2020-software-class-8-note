﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            //3.	用户输入三个数，找出最大的数，打印输出。
            Console.WriteLine("用户输入第一个数：");
            int a = int.Parse(Console.ReadLine());
            Console.WriteLine("用户输入第二个数：");
            int b = int.Parse(Console.ReadLine());
            Console.WriteLine("用户输入第三个数：");
            int c = int.Parse(Console.ReadLine());

            if (a>b && a>c)
            {
                Console.WriteLine("最大值为："+a);
            }
            else if(b>a && b > c) {
                Console.WriteLine("最大值为：" + b);
            }
            else if (c > a && c > b)
            {
                Console.WriteLine("最大值为：" + c);
            }
            Console.ReadKey();
        }
    }
}
