﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp8
{
    class Program
    {
        static void Main(string[] args)
        {
            //8.实现查找数组元素索引的功能。定义一个数组，然后控制台输入
            //要查找的元素，返回输入值在数组中最后一次出现的位置。若是找不到，请打印找不到。

            int[] a = { 5, 1, 8, 3, 4, 5 };
            Console.WriteLine("请输入查找的数：");
            int b = int.Parse(Console.ReadLine());
            int lastIndex = Array.LastIndexOf(a, b);
            if (lastIndex != -1)
            {
                Console.WriteLine(b + "在数组元素中的下标是" + lastIndex);
            }
            else
            {
                Console.WriteLine("找不到");
            }
            Console.ReadKey();
        }
    }
}
