﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace enum_5._14
{
    class Program
    {
        public enum Fruit
        {
            葡萄,草莓,奇异果,苹果,香蕉,哈密瓜,西瓜
        }
        static void Main(string[] args)
        {
            Random random = new Random();
            for (int i = 0; i < 7; i++)
            { 
                int num =random.Next(0, 7);
                Console.WriteLine("随机生成的数为：" + num);
                Fruit fruit = (Fruit)num;
                Fruitscore(fruit);
            }
            Console.ReadKey();
        }

        private static void Fruitscore(Fruit fruit)
        {
            switch (fruit)
            {
                case Fruit.葡萄:
                    Console.WriteLine(fruit+"15分");
                    break;
                case Fruit.草莓:
                    Console.WriteLine(fruit + "10分");
                    break;
                case Fruit.奇异果:
                    Console.WriteLine(fruit + "8分");
                    break;
                case Fruit.苹果:
                    Console.WriteLine(fruit + "8分");
                    break;
                case Fruit.香蕉:
                    Console.WriteLine(fruit + "6分");
                    break;
                case Fruit.哈密瓜:
                    Console.WriteLine(fruit + "4分");
                    break;
                case Fruit.西瓜:
                    Console.WriteLine(fruit + "2分");
                    break;
                default:
                    break;
            }
        }
    }
}
