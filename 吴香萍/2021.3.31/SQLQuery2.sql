create table stuinfo(
   stuNO nvarchar(20) not null,
   stuName nvarchar(50),
   stuAge int not null,
   stuAdress nvarchar(55) not null,
   stuSeat int identity(1, 1),
   stuSex tinyint not null,
);

create table stuexam(
  examNo int not null identity(1, 1),
  stuNO nvarchar(20) not null,
  writtenExam int,
  labExam int,
);

insert into stuinfo(stuNO, stuName, stuAge, stuAdress, stuSex)
	values('s2501','张秋利','20','美国硅谷','1'),
	('s2502','李斯文','18','湖北武汉','0'),
	('s2503','马文才','22','湖南长沙','1'),
	('s2504','欧阳俊雄','21','湖北武汉','0'),
	('s2505','梅超风','20','湖北武汉','1'),
	('s2506','陈旋风','19','美国硅谷','1'),
	('s2507','陈风','20','美国硅谷','0');

insert into stuexam(stuNO, writtenExam, labExam)
	values('s2501','50','70'),
	('s2502','60','65'),
	('s2503','86','85'),
	('s2504','40','80'),
	('s2505','70','90'),
	('s2506','85','90');


	select * from stuinfo;
	select * from stuexam;

	--查询学生信息表（stuinfo）中所有列信息，给每列取上中文名称
	select stuNO 学号,stuName 姓名,stuAge 年龄,stuAdress 地址,stuSeat 序号,stuSex 性别 from stuinfo;

	--查询学生信息表（stuinfo）中的姓名，年龄和地址三列的信息
	select top 3 * from stuinfo;

	--查询学生分数表（stuexam）中的学号，笔试和机试三列的信息，并为这三列取中文名字,注意：要用三种方法
	select stuNo 学号, writtenExam 笔试, labExam 机试 from stuexam 
	select stuNo as 学号, writtenExam as 笔试, labExam as 机试 from stuexam 
	select 学号=stuNo ,  笔试=writtenExam, 机试=labExam  from stuexam 

	--查询学生信息表（stuInfo）中的学号，姓名，地址，以及将：姓名+@+地址 组成新列 “邮箱”
	select stuNO 学号, stuName 名字, stuAdress 地址, stuName+'@'+stuAdress 邮箱 from stuinfo;

	--查询学生分数表（stuexam）中的学生的学号，笔试，机试以及总分这四列的信息
	select stuNO 学号, writtenExam 笔试, labExam 机试, writtenExam + labExam 总分 from stuexam 