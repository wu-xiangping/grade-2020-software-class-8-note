﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1
{
    class Staff
    {
        public int id;
        public string name;
        public string sex;
        public string degree;
        public string department;
 
        public Staff()
        {
            degree = "专科";
        }
        public Staff(int id, string name, string sex, string degree, string department)
        {
            this.id = id;
            this.name = name;
            this.sex = sex;
            this.degree = degree;
            this.department = department;
        }

        public void MM()
        {
            Console.WriteLine("工号："+id);
            Console.WriteLine("姓名："+name);
            Console.WriteLine("性别："+sex);
            Console.WriteLine("学历："+degree);
            Console.WriteLine("部门："+department);
        }
    }
}
