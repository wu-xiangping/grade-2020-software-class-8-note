﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App3
{
    class SumUtils
    {
        public static int Sum(int a, int b)
        {
            return a + b;
        }
        public static double Sum(double c, double d)
        {
            return c + d;
        }
        public static string Sum(string e, string f)
        {
            return e + f;
        }
        public static int Sum(int a)
        {
            int b = 0;
            for (int i = 1; i <= a; i++)
            {
                b += i;
            }
            return b;
        }

    }
}
