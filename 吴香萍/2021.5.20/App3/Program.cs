﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App3
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 5;
            int b = 20;
            double c = 10.5;
            double d = 20.6;
            string e = "mm";
            string f = "ww";
          
            Console.WriteLine(a + "+" + b + "=" + SumUtils.Sum(a, b));
            Console.WriteLine(c + "+" + d + "=" + SumUtils.Sum(c, d));
            Console.WriteLine(e + "+" + f + "=" + SumUtils.Sum(e, f));
            Console.WriteLine("1 到指定整数的和为 ："+SumUtils.Sum(a));
            Console.ReadKey();
        }
    }
}
