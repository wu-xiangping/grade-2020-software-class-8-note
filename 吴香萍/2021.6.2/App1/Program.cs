﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1
{
    class Program
    {
        static void Main(string[] args)
        {
            string str = "与其他面向对象语言一样，C# 语言也具有面向对象语言的基本特征，即封装、继承、 多态。封装：就是将代码看作一个整体，例如使用类、方法、接口等。在使用定义好的类、 方法、接口等对象时不必考虑其细节，只需要知道其对象名以及所需要的参数即可，也是一种提升代码安全性的方法。继承：是一种体现代码重用性的特性，减少代码的冗余，但在 C# 语言中仅支持单继承。多态：不仅体现了代码的重用性，也体现了代码的灵活性，它主要通过继承和实现接口的方式，让类或接口中的成员表现出不同的作用。";
            //使用循环遍历的方法来实现。
            int a = 0;
            int b = 0;
            for (int i = 0; i < str.Length; i++)
            {
                if (str[i].Equals('类'))
                {
                    a++;     
                }else if (str[i].Equals('码'))
                {
                    b++;
                }               
            }
            Console.WriteLine("类的次数是：" + a);
            Console.WriteLine("码的次数是：" + b);


            //用Replace方法来实现
            //Replace:返回一个新的字符串，用于将指定字符串替换给原字符串中指定的字符串|
           string str1 = str.Replace("类", "");
           string str2 = str.Replace("码", "");
           Console.WriteLine("类的次数是：" + (str.Length-str1.Length));
           Console.WriteLine("码的次数是：" + (str.Length - str2.Length));


            //使用Split()方法来实现。
            //Split:返回一个字符串类型的数组，根据指定的字符数组或者字符串数组中的字符 或字符串作为条件拆分字符串|
            char[] split = new char[] { '类'};
            char[] split1 = new char[] { '码' };
            string[] str3 = str.Split(split);
            string[] str4 = str.Split(split1);
            Console.WriteLine("类的次数是：" + (str3.Length-1));
            Console.WriteLine("码的次数是：" + (str4.Length - 1));

            Console.ReadKey();
        }
    }
}
