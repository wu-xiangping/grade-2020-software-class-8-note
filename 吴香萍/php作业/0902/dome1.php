<?php
$searchList = [
    ["name" => "百度", "url" => "https://www.baidu.com", "score" => "70"],
    ["name" => "谷歌", "url" => "https://www.google.com", "score" => "90"],
    ["name" => "360搜索", "url" => "https://www.so.com", "score" => "62"],
    ["name" => "搜搜", "url" => "https://www.soso.com", "score" => "65"],
];

?>

<html>
<head>
    <meta charset="utf-8">
    <title></title>
</head>
<body>
<table border="1" cellspacing="0" style="border-collapse: collapse">
    <tr>
        <th>序号</th>
        <th>网站名</th>
        <th>url地址</th>
    </tr>
<?php foreach ($searchList as $key => $value):  ?>
    <tr>
        <td><?php echo $key ?></td>
        <td><?php echo $value["name"] ?></td>
        <td><?php echo $value["url"] ?></td>
    </tr>
<?php endforeach;  ?>

</table>
</body>
</html>
