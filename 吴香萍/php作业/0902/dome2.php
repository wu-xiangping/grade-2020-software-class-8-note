<?php
//1. 获取今日开始的时间戳。
date_default_timezone_set("PRC");
$time=strtotime("2021-9-2 00:00:00");
echo $time;
echo "<br/>";

//2. 获取本周开始的时间戳。
date_default_timezone_set("PRC");
$time=strtotime("2021-8-29 00:00:00");
echo $time;
echo "<br/>";

//3. 获取本月开始的时间戳。
date_default_timezone_set("PRC");
$time=strtotime("2021-9-1 00:00:00");
echo $time;
echo "<br/>";

//4. 时间戳到2038年的问题是什么？
//超过2038年1月19日的时间戳，数据库里直接为0
date_default_timezone_set("PRC");
$time=strtotime("2038-1-1 00:00:00");
echo $time;