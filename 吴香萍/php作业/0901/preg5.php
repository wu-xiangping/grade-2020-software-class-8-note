<?php
//5. 实务中注册经常需要填写密码，我们需要对密码进行验证，参考如下规则，并用php代码实现。
//	1. 使用正则表达式判断密码只包含数字，并且长度在6~15位。
//	2. 使用正则表达式判断密码只包含数字和字母，并且既有数字也有字母，长度在6~15位。（高能预警）
//	3. 使用正则表达式判断密码只包含数字、字母和特殊字符~!@#$%^&*，并且数字、字母、特殊字符至少包含1个，长度在6~15位。（高能预警）
$str='1234567';
$str1='1234567sad';
$str2='12345wdf!';
$result=preg_match("/^\d{6,15}$/",$str);

$result1=preg_match("/^\[0-9a-zA-Z]{6,15}$/",$str1);

$result2=preg_match("/[~!@#$%^&*]/",$str2);
$result3=preg_match("/[0-9a-zA-Z]/",$str2);
$result4=preg_match("/^[0-9a-zA-Z~!@#$%^&*]{6,15}$/",$str2);
if ($result>0){
    echo "密码符合条件";
}else{
    echo "密码不符合条件";
}
echo "<br/>";
if ($result1>0){
    echo "密码符合条件";
}else{
    echo "密码不符合条件";
}
echo "<br/>";
if ($result2>0&&$result3>0&&$result4>0){
    echo "密码符合条件";
}else{
    echo "密码不符合条件";
}