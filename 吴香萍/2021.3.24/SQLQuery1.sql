--创建 银行数据库 Bank
create database Bank;

--创建 账号信息表 AccountInfo
create table AccountInfo(
AccountId int not null identity(1,1),
AccountCode varchar(20) not null,
AccountPhone varchar(20),
RealName varchar(20) not null,
);

--创建 银行卡表 BankCard
create table BankCard(
CardNo varchar(30) primary key,
AccountId int not null,
CardPwd varchar(30) not null,
CardBalance money not null,
CardState tinyint not null,
CardTime varchar(30) not null,
);

 --创建 交易信息表 CardExchange
 create table CardExchange(
 ExchangeId int primary key identity(1,1),
 CardNo varchar(30) not null,
 MoneyInBank money not null,
 MoneyOutBank money not null,
 ExchangeTime smalldatetime not null,
  );

--给 账号信息表 AccountInfo 增加字段
alter table AccountInfo add OpenTime smalldatetime not null;

--给 银行卡表 BankCard 修改字段：默认值为getdate()
alter table BankCard alter column CardTime smalldatetime ;
alter table BankCard add constraint DF_BankCard_CardTime default(getdate()) for CardTime;

--AccountInfo.AccountId 设置为主键。
alter table AccountInfo add constraint PK_AccountInfo_AccountId primary key (AccountId);

--AccountInfo.AccountCode 设置为唯一.
alter table AccountInfo add constraint UQ_AccountInfo_AccountCode unique(AccountCode);

--AccountInfo.OpenTime 默认值设置为getdate()。
alter table AccountInfo add constraint DF_AccountInfo_OpenTime default(getdate()) for OpenTime;

--AccountInfo.AccountPhone 设置为非空。
alter table AccountInfo alter column AccountPhone nvarchar(20) not null;

--BankCard.CardBalance 默认值设置为0.00
alter table BankCard add constraint DF_BankCard_CardBalance default(0.00) for CardBalance;

--BankCard.CardState 默认值设置为1
alter table BankCard add constraint DF_BankCard_CardState default(1) for CardState;

--BankCard.AccountId 设置为 AccountInfo.AccountId 的外键
alter table BankCard add constraint FK_BankCard_AccountId foreign key (AccountId) references AccountInfo(AccountId);

--CardExchange.CardNo 设置为 BankCard.CardNo 的外键。
alter table CardExchange add constraint FK_CardExchange_CardNo foreign key (CardNo) references BankCard(CardNo);

--CardExchange.MoneyInBank 设置要大于等于0
alter table CardExchange add constraint CK_CardExchange_MoneyInBank check(MoneyInBank>=0);

--CardExchange.MoneyOutBank 设置要大于等于0。
alter table CardExchange add constraint CK_CardExchange_MoneyOutBank check(MoneyInBank>=0);