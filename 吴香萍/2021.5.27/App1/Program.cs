﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1
{
    class Program
    {
        static void Main(string[] args)
        {
            Cat cat = new Cat("凯猫","鱼");
            cat.Eat();
            cat.climb();
            Console.WriteLine();

            Dog dog = new Dog("特狗", "骨头");
            dog.Eat();
            dog.Swimming();
            Console.WriteLine();

            Duck duck = new Duck("唐鸭","菜叶");
            duck.Eat();
            duck.Swimming();
            Console.WriteLine();

            Monkey monkey = new Monkey("猴子","香蕉");
            monkey.Eat();
            monkey.climb();

            Console.ReadKey();
        }
    }
}
