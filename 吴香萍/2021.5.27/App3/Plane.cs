﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App3
{
    class Plane : Vehicle, IFlyable
    {
      

        public void Fly()
        {
            Console.WriteLine("飞机正在起飞");
        }

        public void Land()
        {
            Console.WriteLine("飞机正在飞行中");
        }

        public void TakeOff()
        {
            Console.WriteLine("飞机正在着陆");
        }
    }
}
