﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App3
{
    interface IFlyable
    {
        //起飞、飞行中、着陆的方法接口
        void TakeOff();
        void Fly();
        void Land();
    }
}
