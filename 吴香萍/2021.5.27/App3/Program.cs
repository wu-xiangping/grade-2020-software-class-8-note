﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App3
{
    class Program
    {
        static void Main(string[] args)
        {
            Plane plane = new Plane();
            plane.by();
            plane.Fly();
            plane.Land();
            plane.TakeOff();
            Console.WriteLine();

            Bird bird = new Bird("鸟","虫子");
            bird.Eats();
            bird.Fly();
            bird.Land();
            bird.TakeOff();
            Console.WriteLine();

            Superman superman = new Superman("超人","汉堡");
            superman.Eats();
            superman.Fly();
            superman.Land();
            superman.TakeOff();

            Console.ReadKey();
        }
    }
}
