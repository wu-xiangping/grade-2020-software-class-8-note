﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App3
{
    class Superman : Animal, IFlyable
    {
        public Superman(string Name, string Eat) : base(Name, Eat)
        {
        }

        public void Fly()
        {
            Console.WriteLine("{0}开始飞", Name);
        }

        public void Land()
        {
            Console.WriteLine("{0}正在飞", Name);
        }

        public void TakeOff()
        {
            Console.WriteLine("{0}正在着陆", Name);
        }
    }
}
