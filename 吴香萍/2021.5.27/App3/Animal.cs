﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App3
{
    //鸟和超人的父类
    class Animal
    {
        public string Name;
        public string Eat;

        public Animal( string Name,string Eat)
        {
           this.Name = Name;
           this.Eat = Eat;
        }

        public void Eats()
        {
            Console.WriteLine("{0}正在吃{1}",Name,Eat);
        }
    }
}
