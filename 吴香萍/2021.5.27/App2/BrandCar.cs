﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App2
{
    class BrandCar : Cars, IFly
    {
        public BrandCar(string Brand, string Running) : base(Brand, Running)
        {

        }

        public override void Ran()
        {
            Console.WriteLine("{0}是{1}开动的", Brand, Running);
        }

        public void Fly()
        {
            Console.WriteLine("{0}是{1}飞的", Brand, Running);
        }
    }
}
