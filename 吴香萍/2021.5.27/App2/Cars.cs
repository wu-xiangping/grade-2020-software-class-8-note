﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App2
{
    class Cars
    {
        public string Brand;
        public string Running;

        public Cars(string Brand, string Running)
        {
           this.Brand = Brand;
           this.Running = Running;
        }

        public virtual void Ran()
        {
            
        }
    }
}
