﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App2
{
    class Student
    {
        public string id;
        public string name;
        public string sex;
        private int age;
        public string major;

        public int Age
        {
            get
            {
                return age;
            }
            set
            {
                if (value>0 && value<128)
                {
                    age = value;
                }
                else
                {
                    age = 0;
                }
            }            
        }
        public void stu()
        {
            Console.WriteLine("学号："+id);
            Console.WriteLine("姓名："+name);
            Console.WriteLine("性别："+sex);
            Console.WriteLine("年龄："+age);
            Console.WriteLine("专业信息："+major);
        }
    }
}
