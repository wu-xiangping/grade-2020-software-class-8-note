﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App3
{
    class Book
    {
        public string id;
        public string name;
        private int price;
        public string pub;
        public string index;

        public int Price
        {
            get
            {
                return price;
            }
            set
            {
                if (value>0)
                {
                    price = value;
                }
                else
                {
                    price = 0;
                }
            }
        }

        public void Mg()
        {
            Console.WriteLine("图书的编号："+id);
            Console.WriteLine("图书的书名："+name);
            Console.WriteLine("图书的价格："+price);
            Console.WriteLine("图书的出版社："+pub);
            Console.WriteLine("图书的作者信息："+index);
        }
    }
}
