-- 创建一个库
create database Task;
go
-- 使用这个库
use Task;
go
create table Task (
	TaskId int not null identity(1, 1), -- 任务id
	TaskName nvarchar(50), -- 任务名称
	TaskContent nvarchar(2000), -- 任务内容
	TaskStatus tinyint not null default(1), -- 任务状态：1新创建；2表示进行中；3表示已完成；
	TaskCreateTime smalldatetime default(getdate()), -- 任务创建时间
	TaskUpdateTime smalldatetime default(getdate()), -- 任务修改时间
);
insert into Task (TaskName,TaskContent) 
values('这是第一个任务','任务一：');
select * from Task
go
create table Admin (
	AdminId int not null identity(1, 1), -- 任务id
	AdminAccount nvarchar(50), -- 管理员账号
	AdminPassword varchar(20) not null, -- 管理员密码
	AdminCreateTime smalldatetime default(getdate()), -- 创建时间
);
insert into Admin (AdminAccount, AdminPassword) values('admin', '123456');
go

