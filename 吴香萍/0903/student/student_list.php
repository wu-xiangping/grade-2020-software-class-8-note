<?php

// 连接数据库，查询出所有的班级信息
$dsn = "sqlsrv:Server=localhost;Database=Student";
$db = new PDO($dsn, "sa", "123456");

$sql = 'select * from Student order by StudentId desc';
$result = $db->query($sql);
$StudentList = $result->fetchAll(PDO::FETCH_ASSOC);
//var_dump($StudentList);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>列表</title>
    <link rel="stylesheet" type="text/css" href="css/main.css"/>
</head>
<body>
<div id="container">
    <a id="add" href="student_add.php">增加</a>
    <table class="list">
        <tr>
            <th>学生id</th>
            <th>学生名称</th>
            <th>班级名称</th>
            <th>性别</th>
            <th>生日</th>
            <th>地址</th>
            <th>操作</th>
        </tr>
        <?php foreach ($StudentList as $key => $value): ?>
            <tr>
                <td><?php echo $value['StudentId']; ?></td>
                <td><?php echo $value['StudentName']; ?></td>
                <td>
                    <?php
                    $classId = $value['ClassId'];
//                    echo $c
                    if ($classId) {
                        $sql = 'select * from Class where ClassId='.$classId;
//                        echo $sql;

                        $result = $db->query($sql);
                        $classInfo = $result->fetch(PDO::FETCH_ASSOC);
//                    var_dump($classInfo);
                        echo $classInfo['ClassName'];
                    } else {
                        echo "班级id错误";
                    }
                    ?>
                </td>
                <td><?php
                    if ($value['StudentSex'] == 1) {
                        echo '男';
                    } else if ($value['StudentSex'] == 2) {
                        echo '女';
                    } else {
                        echo '未知';
                    }
                    ?></td>
                <td><?php echo $value['StudentBirth']; ?></td>
                <td><?php echo $value['StudentAddress']; ?></td>
                <td>
                    <a href="student_update.php?student_id=<?php echo $value['StudentId']; ?>">修改</a>
                    <a href="student_delete.php?student_id=<?php echo $value['StudentId']; ?>">删除</a>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
</div>
</body>
</html>
