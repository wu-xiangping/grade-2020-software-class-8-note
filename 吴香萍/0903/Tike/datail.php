<?php
$TaskId=$_GET['task_id'];
$dsn = "sqlsrv:Server=localhost;Database=Task";
$db = new PDO($dsn, "sa", "123456");

$sql = "select * from Task where TaskId='{$TaskId}'";
$result = $db->query($sql);
$TaskList = $result->fetchAll(PDO::FETCH_ASSOC);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>任务详情</title>
    <link rel="stylesheet" type="text/css" href="css/main.css"/>
    <script src="js/jquery.js"></script>
</head>
<body>
<div id="container">
    <a href="list.php">返回任务列表</a>
    <form>
        <table class="update">
            <caption>
                <h3>任务详情</h3>
            </caption>
            <tr>
                <td>任务名称：</td>
                <td>这个是一个任务</td>
            </tr>
            <tr>
                <td>任务状态：</td>
                <td>已完成</td>
            </tr>
            <tr>
                <td>任务内容：</td>
                <td><textarea cols="60" rows="15"
                              readonly="readonly">完成修改h5页面的logo，项目地址：http://xxx</textarea></td>
            </tr>
            <?php foreach ($TaskList as $key=>$value): ?>
            <tr>
                <td></td>
                <td>
                    <?php
                    if ($value['TaskStatus']==3) {
                        echo "<a class=\"update_ing\"  href=\"javascript:;\" onclick=\"confirm('标记为进行中?')?location.href='list.php':''\" >标记为进行中</a> ";
                        $dsn = "sqlsrv:Server=localhost;Database=Task";
                        $db = new PDO($dsn, "sa", "123456");
                        $sql1 = "update Task set TaskStatus='2' where TaskId='{$TaskId}'";
                        $result1=$db->exec($sql1);
                    }    else if ($value['TaskStatus']==2){
                        echo "<a class=\"update_ing\"  href=\"javascript:;\" onclick=\"confirm('标记为已完成?')?location.href='list.php':''\">标记为已完成</a>";
                        $dsn = "sqlsrv:Server=localhost;Database=Task";
                        $db = new PDO($dsn, "sa", "123456");
                        $sql1 = "update Task set TaskStatus='3' where TaskId='{$TaskId}'";
                        $result1=$db->exec($sql1);
                    }
                    ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    </form>
</div>
<script src="js/main.js"></script>
</body>
</html>

