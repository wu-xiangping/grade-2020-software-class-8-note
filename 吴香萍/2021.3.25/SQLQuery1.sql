create database Student;

create table Class(
	ClassId int not null identity(1,1),
	ClassName nvarchar(50) not null
);

create table Student (
	StudentId int not null identity(1, 1),
	StudentName nvarchar(50),
	StudentSex tinyint not null,
	StudentBirth date,
	StudentAddress nvarchar(255) not null,
	ClassId int default(''),
);

create table Course(
	CourseId int identity(1,1),
	CourseName nvarchar(50),
	CourseCredit int
);

create table ClassCourse(
	ClassCourseId int identity(1,1),
	ClassId int,
	CourseId int
);

create table Score(
	ScoreId int identity(1,1),
	StudentId int,
	CourseId int,
	Score int
);

insert into Class (ClassName) values ('软件一班');
insert into Class (ClassName) values ('软件二班');
insert into Class (ClassName) values ('计算机应用技术班');


insert into Student (StudentName, StudentSex, StudentBirth, StudentAddress, ClassId) 
    values ('刘正',1,'2000-01-01','广西省桂林市七星区空明西路10号鸾东小区',1),
		   ('黄贵',1,'2001-03-20','江西省南昌市青山湖区艾溪湖南路南150米广阳小区', 1),
           ('陈美',2,'2000-07-08','福建省龙岩市新罗区曹溪街道万达小区',1);
insert into Student (StudentName, StudentSex, StudentBirth, StudentAddress, ClassId)
	values ('江文',1,'2000-08-10','安徽省合肥市庐阳区四里河路与潜山路交汇处万科城市之光',2),
	       ('钟琪',2,'2001-03-21','湖南省长沙市雨花区红花坡社区',2);
insert into Student (StudentName, StudentSex, StudentBirth, StudentAddress, ClassId)
	values('曾小林',1,'1999-12-10','安徽省合肥市庐阳区四里河路与潜山路交汇处万科城市之光',3),
		  ('欧阳天天',2,'2000-04-05','湖北省武汉市洪山区友谊大道与二环线交汇处融侨悦府',3),
		  ('徐长卿',1,'2001-01-30','江苏省苏州市苏州工业园区独墅湖社区',3),
		  ('李逍遥',1,'1999-11-11','广东省广州市白云区金沙洲岛御洲三街恒大绿洲',3);
insert into Student (StudentName, StudentSex, StudentBirth, StudentAddress)
	values('东方不败',3,'1999-12-11','河北省平定州西北四十余里的猩猩滩'),
		  ('令狐冲',1,'2000-08-11','陕西省渭南市华阴市玉泉路南段');

insert into Course (CourseName, CourseCredit)
	values('数据库高级应用','3'),
		  ('javascript编程基础','3'),
		  ('web前端程序设计基础','4'),
		  ('动态网页设计.net基础','6');
insert into ClassCourse (ClassId, CourseId)
	values(1,1),
		  (1,2),
		  (1,3),
		  (1,4);
insert into ClassCourse (ClassId, CourseId)
	values(2,1),
		  (2,2),
		  (2,3),
		  (2,4);
insert into Course(CourseName, CourseCredit) values ('动态网页设计php基础', 6);
insert into ClassCourse (ClassId, CourseId)
	values(3,1),
		  (3,2),
		  (3,3),
		  (3,5);

insert into Score(StudentId, CourseId, Score)
	values(1,1,80),
		  (1,2,78),
		  (1,3,65),
		  (1,4,90),
		  (2,1,60),
		  (2,2,77),
		  (2,3,68),
		  (2,4,88),
		  (3,1,88),
		  (3,2,45),
		  (3,3,66),
		  (3,4,75),
		  (4,1,56),
		  (4,2,80),
		  (4,3,75),
		  (4,4,66),
		  (5,1,88),
		  (5,2,79),
		  (5,3,72),
		  (5,4,85),
		  (6,1,68),
		  (6,2,88),
		  (6,3,73),
		  (6,5,63),
		  (7,1,84),
		  (7,2,90),
		  (7,3,92),
		  (7,5,78),
		  (8,1,58),
		  (8,2,59),
		  (8,3,65),
		  (8,5,75),
		  (9,1,48),
		  (9,2,67),
		  (9,3,71),
		  (9,5,56),
		  (9,5,56);
select * from Class;
select * from Student;
select * from Course;
select * from ClassCourse;
select * from Score;

select * from Course order by CourseCredit desc;
select * from Score order by Score asc;
select * from Student order by StudentBirth desc;

select * from Student where ClassId = 1;
select * from Student where StudentSex = 2;
select * from Student where StudentBirth >= '2000-01-01' and StudentBirth < '2001-01-01';